package com.MMOCardGame.GameServer.Tester;

import lombok.Getter;

public class TestCase {
    @Getter
    public String name = "";
    @Getter
    public boolean ignored = false;

    public TestCase() {
    }

    public TestCase(String name) {
        this.name = name;
    }

    public TestCase(String name, boolean ignored) {
        this.name = name;
        this.ignored = ignored;
    }

    @Override
    public String toString() {
        return name;
    }
}
