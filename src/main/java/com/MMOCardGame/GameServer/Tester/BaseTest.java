package com.MMOCardGame.GameServer.Tester;

public abstract class BaseTest {
    protected int timeoutMillis = 1000;
    protected String workspace = null;
    private StringBuilder testOutput = new StringBuilder();

    public void initWorkspace(String workspace) throws Throwable {
        this.workspace = workspace;
    }

    public abstract void initCase(TestCase testCase) throws Throwable;

    public void initServer() throws Throwable {
    }

    public void cleanup() throws Throwable {
    }

    public abstract void runTest() throws Throwable;

    public abstract ServerProvider getServerProvider();

    public String getOutput() {
        return testOutput.toString();
    }

    protected void printToOutput(String text) {
        testOutput.append(text);
    }

    protected void printLineToOutput(String text) {
        testOutput.append(text);
        testOutput.append("\n");
    }

    public void finallyCleanup() throws Throwable {
    }
}
