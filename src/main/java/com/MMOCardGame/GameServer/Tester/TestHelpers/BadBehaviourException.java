package com.MMOCardGame.GameServer.Tester.TestHelpers;

public class BadBehaviourException extends RuntimeException {
    public BadBehaviourException() {
    }

    public BadBehaviourException(String message) {
        super(message);
    }

    public BadBehaviourException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadBehaviourException(Throwable cause) {
        super(cause);
    }

    public BadBehaviourException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
