package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.Tester.BaseTest;
import com.MMOCardGame.GameServer.Tester.ServerProvider;
import com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories.DaggerInjectGameSessionManagerFactory;
import com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories.InjectGameSessionManagerProvider;
import com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories.InjectedGameSessions;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Files;

public abstract class GrpcServerTest extends BaseTest {
    protected int gamePort;
    protected int servicePort;
    private ServerProvider serverProvider;

    protected static ServerProvider getProcessServerProvider() {
        return new ProcessServerProvider();
    }

    protected static ServerProvider getInjectionServerProvider(InjectedGameSessions injectedGameSessions) {
        ThreadServerProvider serverProvider = new ThreadServerProvider();
        serverProvider.setGameSessionManagerFactory(DaggerInjectGameSessionManagerFactory
                .builder()
                .injectGameSessionManagerProvider(new InjectGameSessionManagerProvider().setInjectedGameSessions(injectedGameSessions))
                .build());
        return serverProvider;
    }

    protected abstract ServerProvider initServerProvider();

    @Override
    public void initWorkspace(String workspace) throws Throwable {
        super.initWorkspace(workspace);
        serverProvider = initServerProvider();
        copyResources(workspace);
        serverProvider.setWorkspace(workspace);
        gamePort = findAvailablePort();
        servicePort = findAvailablePort();
        serverProvider.setGamePort(gamePort);
        serverProvider.setServicePort(servicePort);
    }

    protected int findAvailablePort() {
        int port = -1;
        while (port == -1) {
            try {
                ServerSocket s = new ServerSocket(0);
                port = s.getLocalPort();
                s.close();
            } catch (IOException ignored) {
            }
        }
        return port;
    }

    private void copyResources(String workspace) {
        serverProvider.getMapOfFilesToCopy().forEach((source, destination) -> {
            try {
                Files.copy(new File(source).toPath(), new File(workspace + "/" + destination).toPath());
            } catch (IOException e) {
            }
        });
    }

    @Override
    public ServerProvider getServerProvider() {
        return serverProvider;
    }

    @Override
    public void cleanup() throws Throwable {
        super.cleanup();
        serverProvider.getMapOfFilesToCopy().forEach((source, destination) -> new File(workspace + "/" + destination).delete());
    }
}

