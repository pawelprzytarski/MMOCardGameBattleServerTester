package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Tester.AssertionException;
import com.MMOCardGame.GameServer.common.CallbackTask;
import com.google.protobuf.Descriptors;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.Nonnull;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GrpcClient {
    private ConcurrentLinkedQueue<StreamingMessage> messages = new ConcurrentLinkedQueue<>();
    private UserServerGrpc.UserServerBlockingStub blockingStub;
    private UserServerGrpc.UserServerStub asyncStub;
    private ManagedChannel channel;
    @Setter
    private CallbackTask<String> debugInfoCallback = s -> {
    };
    @Setter
    private Hello autoHeader = null;
    @Setter
    @Getter
    private UserData userData;
    @Setter
    @Getter
    private int userId;
    @Getter
    private int currectPlayer;
    @Getter
    private GamePhase currentPhase;

    public void start(int port) {
        channel = ManagedChannelBuilder.forAddress("localhost", port)
                .usePlaintext(true)
                .build();
        blockingStub = UserServerGrpc.newBlockingStub(channel);
        asyncStub = UserServerGrpc.newStub(channel);
    }

    public void shutdown() {
        channel.shutdown();
    }

    public Iterator<StreamingMessage> returningMessagesStream() {
        return returningMessagesStream(autoHeader);
    }

    public Iterator<StreamingMessage> returningMessagesStream(Hello request) {
        long start = System.currentTimeMillis();
        asyncStub.returningMessagesStream(request, new StreamObserver<StreamingMessage>() {
            @Override
            public void onNext(StreamingMessage streamingMessage) {
                messages.add(streamingMessage);
            }

            @Override
            public void onError(Throwable throwable) {
                throw new RuntimeException(throwable);
            }

            @Override
            public void onCompleted() {
            }
        });
        long end = System.currentTimeMillis();
        debugInfoCallback.run("connect: " + (end - start) + "ms");
        return messages.iterator();
    }

    public void consumeMessages(int timeoutMilliseconds, int countMessagesToConsume) {
        for (int i = 0; i < countMessagesToConsume; i++)
            waitAndGetStreamingMessage(timeoutMilliseconds);
    }

    public void verifyNoMessages() {
        assertEquals(0, messages.size(), "Unexpected state: Some messages in messages queue. Expected empty messages queue");
    }

    public <U> U waitFor(int timeoutMillis, Class<U> uClass) {//wait for first message and if it is not of type uClass throw exception
        long startedWaiting = System.currentTimeMillis();
        StreamingMessage message = waitAndGetStreamingMessage(timeoutMillis);
        U obj = checkIsCorrectMessageAndGet(uClass, message);
        long endedWaiting = System.currentTimeMillis();
        debugInfoCallback.run("waitFor<" + uClass.getSimpleName() + "> duration: " + (endedWaiting - startedWaiting) + "ms");
        return obj;
    }

    private <U> U checkIsCorrectMessageAndGet(@Nonnull Class<U> uClass, @Nonnull StreamingMessage message) {
        Descriptors.FieldDescriptor fieldDescriptor = getFieldDescriptorForMessage(uClass);
        if (fieldDescriptor != null && message.getType() == fieldDescriptor.getIndex())
            return (U) message.getField(fieldDescriptor);
        else
            throw new AssertionException("StreamingMessages stream return different message than wanted. Wanted: <" + fieldDescriptor.getIndex() + "> but was <" + message.getType() + ">");
    }

    private StreamingMessage waitAndGetStreamingMessage(int timeout) {
        long started = System.currentTimeMillis();
        boolean test = messages.isEmpty();
        while (messages.isEmpty() && started + timeout > System.currentTimeMillis()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
        if (!messages.isEmpty()) {
            StreamingMessage obj = messages.poll();
            debugInfoCallback.run(Date.from(Instant.now()) + " Received message: " + obj);
            handleInfoMessages(obj);
            return obj;
        } else throw new AssertionException("Wait for message timeout");
    }

    private void handleInfoMessages(StreamingMessage obj) {
        if(obj.hasPhaseChanged()){
            currectPlayer=obj.getPhaseChanged().getNewPlayer();
            currentPhase=obj.getPhaseChanged().getNewPhase();
        }
    }

    public <U> U waitUntil(int timeoutMillis, Class<U> uClass) {//wait for message of type uClass. other messages are ignored
        Descriptors.FieldDescriptor fieldDescriptor = getFieldDescriptorForMessage(uClass);
        StreamingMessage streamingMessage = null;
        long startedWaiting = System.currentTimeMillis();
        while (streamingMessage == null) {
            long started = System.currentTimeMillis();
            streamingMessage = waitAndGetStreamingMessage(timeoutMillis);
            timeoutMillis -= System.currentTimeMillis() - started;
            if (streamingMessage.getType() != fieldDescriptor.getIndex())
                streamingMessage = null;
        }
        long endedWaiting = System.currentTimeMillis();
        debugInfoCallback.run("WaitUntil<" + uClass.getSimpleName() + "> duration: " + (endedWaiting - startedWaiting) + "ms");
        return (U) streamingMessage.getField(fieldDescriptor);
    }

    private Descriptors.FieldDescriptor getFieldDescriptorForMessage(Class<?> message) {
        String className = message.getSimpleName();
        Descriptors.FieldDescriptor descriptor = null;
        for (Descriptors.FieldDescriptor fieldDescriptor : StreamingMessage.getDescriptor().getFields()) {
            if (fieldDescriptor.getName().equals(className)) {
                descriptor = fieldDescriptor;
                break;
            }
        }
        return descriptor;
    }

    public ConnectResponse connect(Hello request) {
        long start = System.currentTimeMillis();
        ConnectResponse response = blockingStub.connect(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("connect: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    private String toStringOrEmptyString(Object obj) {
        if (obj == null)
            return "";
        else return obj.toString();
    }

    public void verifyConnect(Hello request, ConnectResponse expectedResponse) {
        assertEquals(expectedResponse, connect(request));
    }

    public ConnectResponse reconnect(ReconnectHello.Builder request) {
        return reconnect(request.setInnerToken(autoHeader.getUserPasswordToken()).setUserName(autoHeader.getUserName()).build());
    }

    public ConnectResponse reconnect(ReconnectHello request) {
        long start = System.currentTimeMillis();
        ConnectResponse response = blockingStub.reconnect(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("reconnect: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyReconnect(ReconnectHello.Builder request, ConnectResponse expectedResponse) {
        verifyReconnect(request.setInnerToken(autoHeader.getUserPasswordToken()).setUserName(autoHeader.getUserName()).build(), expectedResponse);
    }

    public void verifyReconnect(ReconnectHello request, ConnectResponse expectedResponse) {
        assertEquals(expectedResponse, reconnect(request));
    }

    public SimpleResponse surrender() {
        return surrender(autoHeader);
    }

    public SimpleResponse surrender(Hello request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.surrender(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("surrender: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifySurrender(SimpleResponse expectedResponse) {
        verifySurrender(autoHeader, expectedResponse);
    }

    public void verifySurrender(Hello request, SimpleResponse expectedResponse) {
        assertEquals(expectedResponse, surrender(request));
    }

    public GameStateMessage getAllGameState() {
        return getAllGameState(autoHeader);
    }

    public GameStateMessage getAllGameState(Hello request) {
        long start = System.currentTimeMillis();
        GameStateMessage response = blockingStub.getAllGameState(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("getAllGameState: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyGetAllGameState(GameStateMessage expectedResponse) {
        verifyGetAllGameState(autoHeader, expectedResponse);
    }

    public void verifyGetAllGameState(Hello request, GameStateMessage expectedResponse) {
        assertEquals(expectedResponse, getAllGameState(request));
    }

    public SimpleResponse cancelGame() {
        return cancelGame(autoHeader);
    }

    public SimpleResponse cancelGame(Hello request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.cancelGame(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("cancelGame: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyCancelGame(SimpleResponse expectedResponse) {
        verifyCancelGame(autoHeader, expectedResponse);
    }

    public void verifyCancelGame(Hello request, SimpleResponse expectedResponse) {
        assertEquals(expectedResponse, cancelGame(request));
    }

    public AddedToStackResponse useCard(CardUsed request) {
        return useCard(CardUsedRequest.newBuilder()
                .setHeader(autoHeader)
                .setMessage(request)
                .build());
    }

    public AddedToStackResponse useCard(CardUsedRequest request) {
        long start = System.currentTimeMillis();
        AddedToStackResponse response = blockingStub.useCard(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("useCard: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyUseCard(CardUsed request, AddedToStackResponse expectedResponse) {
        verifyUseCard(CardUsedRequest.newBuilder()
                        .setHeader(autoHeader)
                        .setMessage(request)
                        .build()
                , expectedResponse);
    }

    public void verifyUseCard(CardUsedRequest request, AddedToStackResponse expectedResponse) {
        assertEquals(expectedResponse, useCard(request));
    }

    public SimpleResponse endPhase(PhaseChanged request) {
        return endPhase(PhaseChangedRequest.newBuilder()
                .setHeader(autoHeader)
                .setMessage(request)
                .build());
    }

    public SimpleResponse endPhase(PhaseChangedRequest request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.endPhase(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("endPhase: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyEndPhase(PhaseChanged request, SimpleResponse expectedResponse) {
        verifyEndPhase(PhaseChangedRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifyEndPhase(PhaseChangedRequest request, SimpleResponse expectedResponse) {
        assertEquals(expectedResponse, endPhase(request));
    }

    public AbilityResponse useHeroAbility(HeroAbilityUsed request) {
        return useHeroAbility(HeroAbilityUsedRequest.newBuilder().setMessage(request).setHeader(autoHeader).build());
    }

    public AbilityResponse useHeroAbility(HeroAbilityUsedRequest request) {
        long start = System.currentTimeMillis();
        AbilityResponse response = blockingStub.useHeroAbility(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("useHeroAbility: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyUseHeroAbility(HeroAbilityUsed request, AbilityResponse expectedResponse) {
        verifyUseHeroAbility(HeroAbilityUsedRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifyUseHeroAbility(HeroAbilityUsedRequest request, AbilityResponse expectedResponse) {
        assertEquals(expectedResponse, useHeroAbility(request));
    }

    public SimpleResponse selectAttackersDefenders(AttackersDefendersSelected request) {
        return selectAttackersDefenders(AttackersDefendersSelectedRequest.newBuilder().setMessage(request).setHeader(autoHeader).build());
    }

    public SimpleResponse selectAttackersDefenders(AttackersDefendersSelectedRequest request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.selectAttackersDefenders(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("selectAttackersDefenders: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifySelectAttackersDefenders(AttackersDefendersSelected request, SimpleResponse expectedResponse) {
        verifySelectAttackersDefenders(AttackersDefendersSelectedRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifySelectAttackersDefenders(AttackersDefendersSelectedRequest request, SimpleResponse expectedResponse) {
        assertEquals(expectedResponse, selectAttackersDefenders(request));
    }

    public HandChangedCardsResponse drawCards(HandChanged request) {
        return drawCards(HandChangedCardsRequest.newBuilder().setMessage(request).setHeader(autoHeader).build());
    }

    public HandChangedCardsResponse drawCards(HandChangedCardsRequest request) {
        long start = System.currentTimeMillis();
        HandChangedCardsResponse response = blockingStub.drawCards(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("drawCards: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyDrawCards(HandChanged request, HandChangedCardsResponse expectedResponse) {
        verifyDrawCards(HandChangedCardsRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifyDrawCards(HandChangedCardsRequest request, HandChangedCardsResponse expectedResponse) {
        assertEquals(expectedResponse, drawCards(request));
    }

    public HandChangedCardsResponse discardCards(HandChanged request) {
        return discardCards(HandChangedCardsRequest.newBuilder().setMessage(request).setHeader(autoHeader).build());
    }

    public HandChangedCardsResponse discardCards(HandChangedCardsRequest request) {
        long start = System.currentTimeMillis();
        HandChangedCardsResponse response = blockingStub.discardCards(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("discardCards: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyDiscardCards(HandChanged request, HandChangedCardsResponse expectedResponse) {
        verifyDiscardCards(HandChangedCardsRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifyDiscardCards(HandChangedCardsRequest request, HandChangedCardsResponse expectedResponse) {
        assertEquals(expectedResponse, discardCards(request));
    }

    public SimpleResponse muligan() {
        return muligan(autoHeader);
    }

    public SimpleResponse muligan(Hello request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.muligan(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("muligan: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyMuligan(HandChangedCardsResponse expectedResponse) {
        verifyMuligan(autoHeader, expectedResponse);
    }

    public void verifyMuligan(Hello request, HandChangedCardsResponse expectedResponse) {
        assertEquals(expectedResponse, muligan(request));
    }

    public SimpleResponse replaceCards(HandChanged request) {
        return replaceCards(HandChangedCardsRequest.newBuilder().setMessage(request).setHeader(autoHeader).build());
    }

    public SimpleResponse replaceCards(HandChangedCardsRequest request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.replaceCards(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("replaceCards: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyReplaceCards(HandChanged request, HandChangedCardsResponse expectedResponse) {
        verifyReplaceCards(HandChangedCardsRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifyReplaceCards(HandChangedCardsRequest request, HandChangedCardsResponse expectedResponse) {
        assertEquals(expectedResponse, replaceCards(request));
    }

    public SimpleResponse getStartingHand() {
        return getStartingHand(autoHeader);
    }

    public SimpleResponse getStartingHand(Hello request) {
        long start = System.currentTimeMillis();
        SimpleResponse response = blockingStub.keepStartingHand(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("getStartingHand: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyGetStartingHand(SimpleResponse expectedResponse) {
        verifyGetStartingHand(autoHeader, expectedResponse);
    }

    public void verifyGetStartingHand(Hello request, SimpleResponse expectedResponse) {
        assertEquals(expectedResponse, getStartingHand(request));
    }

    public SimpleResponse doTriggerReaction(TriggerActivated request) {
        return doTriggerReaction(TriggerReactionRequest.newBuilder().setMessage(request).setHeader(autoHeader).build());
    }

    public SimpleResponse doTriggerReaction(TriggerReactionRequest request) {
        long start = System.currentTimeMillis();
        var response = blockingStub.doTriggerReaction(request);
        long end = System.currentTimeMillis();
        debugInfoCallback.run("doTriggerReaction: " + (end - start) + "ms Response: " + toStringOrEmptyString(response));
        return response;
    }

    public void verifyDoTriggerReaction(TriggerActivated request, SimpleResponse expectedResponse) {
        verifyDoTriggerReaction(TriggerReactionRequest.newBuilder().setMessage(request).setHeader(autoHeader).build(), expectedResponse);
    }

    public void verifyDoTriggerReaction(TriggerReactionRequest request, SimpleResponse expectedResponse) {
        assertEquals(expectedResponse, doTriggerReaction(request));
    }
}
