package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.Tester.TestCase;
import com.MMOCardGame.GameServer.common.CallbackTaskWithReturn;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CasesCreator<T extends TestCase> {
    List<Param> params = new ArrayList<>();
    List<CallbackTaskWithReturn<Boolean, T>> filters = new ArrayList<>();
    Class<? extends T> tClass;

    public CasesCreator(Class<? extends T> tClass) {
        this.tClass = tClass;
    }

    public <U> CasesCreator<T> addParam(String fieldName, Collection<U> elements) {
        params.add(new Param<>(elements, fieldName));
        return this;
    }

    public CasesCreator<T> addFilter(CallbackTaskWithReturn<Boolean, T> filter) {
        filters.add(filter);
        return this;
    }

    public List<T> createCases() {
        List<T> result = new ArrayList<>();
        createTestCase(result, 0, new ArrayList<>());
        return result;
    }

    private void createTestCase(List<T> result, int i, List<ParamElement> paramElements) {
        for (Object obj : params.get(i).elements) {
            ParamElement element = new ParamElement(obj, params.get(i).fieldName);
            paramElements.add(element);
            if (i >= params.size() - 1)
                setTestCaseFieldsAndAddToResults(result, paramElements);
            else createTestCase(result, i + 1, paramElements);
            paramElements.remove(element);
        }
    }

    private void setTestCaseFieldsAndAddToResults(List<T> result, List<ParamElement> paramElements) {
        T testCase = createTestCaseInstance();
        for (ParamElement element : paramElements) {
            tryToSetTestCaseField(testCase, element);
        }
        if (filters.stream().allMatch(filter -> testCase != null && filter.run(testCase)))
            result.add(testCase);
    }

    private void tryToSetTestCaseField(T testCase, ParamElement element) {
        try {
            tClass.getField(element.fieldName).set(testCase, element.element);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
            try {
                String tmp = "set" + element.fieldName.substring(0, 1).toUpperCase() + element.fieldName.substring(1);
                tClass.getMethod("set" + element.fieldName.substring(0, 1).toUpperCase() + element.fieldName.substring(1), element.element.getClass())
                        .invoke(testCase, element.element);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException ignored2) {
            }
        }
    }

    private T createTestCaseInstance() {
        try {
            return tClass.newInstance();
        } catch (InstantiationException | IllegalAccessException ignored) {
            return null;
        }
    }

    private class Param<U> {
        Collection<U> elements;
        String fieldName;

        public Param(Collection<U> elements, String fieldName) {
            this.elements = elements;
            this.fieldName = fieldName;
        }
    }

    private class ParamElement<U> {
        U element;
        String fieldName;

        public ParamElement(U element, String fieldName) {
            this.element = element;
            this.fieldName = fieldName;
        }
    }

}
