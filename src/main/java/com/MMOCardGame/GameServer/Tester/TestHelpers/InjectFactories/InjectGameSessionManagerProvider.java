package com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories;

import com.MMOCardGame.GameServer.GameEngine.factories.*;
import dagger.Module;
import dagger.Provides;

@Module(includes = {GameStateBuilderProvider.class, GameRefereeBuilderProvider.class})
public class InjectGameSessionManagerProvider {
    InjectedGameSessions injectedGameSessions;

    public InjectGameSessionManagerProvider setInjectedGameSessions(InjectedGameSessions injectedGameSessions) {
        this.injectedGameSessions = injectedGameSessions;
        return this;
    }

    @Provides
    GameSessionManagerBuilder getGameSessionsManager(GameStateBuilder gameStateBuilder, GameRefereeBuilder gameRefereeBuilder) {
        return new BasicGameSessionManagerBuilder().setGameSessionFactory(new InjectionGameSessionFactory()
                .setInjectedGameSessions(injectedGameSessions)
                .setGameRefereeBuilder(gameRefereeBuilder)
                .setGameStateBuilder(gameStateBuilder));
    }
}
