package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.ConnectResponse;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.Hello;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.UserData;
import com.MMOCardGame.GameServer.Tester.ServerProvider;
import com.MMOCardGame.GameServer.Tester.TestCase;
import com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories.InjectedGameSessions;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public abstract class InitedClientsTest extends GrpcInitedServerTest {
    protected List<GrpcClient> clients = new ArrayList<>();
    private int userCount;

    @Override
    protected ServerProvider initServerProvider() {
        return getInjectionServerProvider(new InjectedGameSessions());
    }

    @Override
    public void initCase(TestCase testCase) {
        CaseArgs args = (CaseArgs) testCase;
        setMatchType(GameSessionSettings.MatchType.Single);
        setSessionId(1);
        userCount = args.userCount;

        List<UserData> userDataList = new ArrayList<>();
        for (int i = 0; i < userCount; i++)
            userDataList.add(getUserInfo(i));

        setUserData(userDataList);
    }

    protected UserData getUserInfo(int i) {
        return UserData.newBuilder()
                .addAllCardIds(getCardIds())
                .setToken("TestToken" + i)
                .setUsername("User" + i)
                .setHeroId(1)
                .build();
    }

    protected List<Integer> getCardIds() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 45; i++)
            list.add(i % 5 + 1);
        return list;
    }

    @Override
    public void initServer() throws Throwable {
        super.initServer();
        initClients();
    }

    protected void initClients() {
        for (int i = 0; i < userCount; i++) {
            UserData userData = getUserInfo(i);
            GrpcClient grpcClient = connectToGameServer();
            ConnectResponse response = sendConnectionMessageAndGetResponse(userData, grpcClient);
            clients.add(grpcClient);
            assertResponse(response);
            grpcClient.setAutoHeader(Hello.newBuilder()
                    .setUserPasswordToken(response.getResponse().getConnectionToken())
                    .setUserName(userData.getUsername())
                    .build());
            grpcClient.setUserData(userData);
            grpcClient.setUserId(response.getGameState().getPlayersList().stream().
                    filter(playerState -> playerState.getName().equals(userData.getUsername()))
                    .map(playerState -> playerState.getId())
                    .findAny()
                    .orElse(-1));
            assertNotNull(grpcClient.returningMessagesStream());
        }
    }

    private ConnectResponse sendConnectionMessageAndGetResponse(UserData userData, GrpcClient grpcClient) {
        return grpcClient.connect(Hello.newBuilder()
                .setUserName(userData.getUsername())
                .setUserPasswordToken(userData.getToken())
                .build());
    }

    private GrpcClient connectToGameServer() {
        GrpcClient grpcClient = new GrpcClient();
        grpcClient.start(gamePort);
        return grpcClient;
    }

    private void assertResponse(ConnectResponse response) {
        assertEquals(0, response.getResponse().getReponseCode());
        assertTrue(response.getResponse().getConnectionToken().length() >= 10);
        assertEquals(userCount, response.getGameState().getPlayersList().size());
        assertTrue(response.getGameState().getStackActionsList().isEmpty());
        assertEquals(-1, response.getGameState().getActivePlayer());
        assertEquals(GamePhase.Waiting, response.getGameState().getCurrentPhase());
    }

    @Override
    public void finallyCleanup() throws Throwable {
        super.finallyCleanup();
        clients.forEach(GrpcClient::shutdown);
    }

    @Data
    public static class CaseArgs extends TestCase {
        public int userCount;
        protected Integer matchType;
    }


}
