package com.MMOCardGame.GameServer.Tester.TestHelpers.InjectFactories;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSession;
import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionManager;
import com.MMOCardGame.GameServer.GameEngine.Sessions.UserSessionEndInfo;
import com.MMOCardGame.GameServer.common.Observable;

public class InjectedGameSessions {
    public boolean contains(int sessionId) {
        return false;
    }

    public GameSession recreateSession(Observable<UserSessionEndInfo> sessionEndObservable, GameSessionManager gameSessionManager) {
        return null;
    }
}
