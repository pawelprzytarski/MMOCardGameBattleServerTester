package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.Tester.ServerProvider;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class ProcessServerProvider extends ServerProvider {
    public static final String ServiceToken = "ServiceToken";
    private final String startedText = "INFO: Await user server";
    private Process process = null;
    private Throwable throwable = null;
    private String readErrorOutput = "";

    @Override
    public void runServer() {
        try {
            String javaHome = System.getProperty("java.home");
            String[] args = ("java -jar GameServer.jar -D userServerPort=" + gamePort +
                    " -D serviceServerPort=" + servicePort + " -D cardsPath=./cards.json -D heroInfoPath=./heros.json" +
                    " -D innerServiceToken=ServiceToken").split(" ");
            args[0] = javaHome + "/bin/java";
            process = new ProcessBuilder(args)
                    .directory(new File(workspace))
                    .start();
            waitUntilProcessStart();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
        }
    }

    private void waitUntilProcessStart() throws InterruptedException, IOException {
        boolean started;
        InputStream stream = process.getErrorStream();
        do {
            Thread.sleep(50);
            if (stream.available() > 0) {
                byte[] bytes = new byte[stream.available()];
                stream.read(bytes);
                readErrorOutput += new String(bytes);
            }
            started = readErrorOutput.contains(startedText);
        } while (!started && process.isAlive());
        if (!started)
            throw new RuntimeException("Server not started");
    }

    @Override
    public void stopServer() {
        if (process.isAlive()) {
            process.destroy();
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                throwable = e;
            }
        }
    }

    @Override
    public Throwable getError() {
        return throwable;
    }

    @Override
    public boolean isError() {
        return throwable != null;
    }

    @Override
    public int exitCode() {
        if (process == null) return 0;
        if (!process.isAlive())
            if (process.exitValue() == 1)
                return 0;
            else return process.exitValue();
        else return -1;
    }

    @Override
    public String getErrorOutput() {
        String output = "";
        try {
            output = readErrorOutput + IOUtils.toString(process.getErrorStream(), Charset.forName("UTF-8"));
        } catch (IOException | NullPointerException ignored) {
        }
        return output;
    }

    @Override
    public String getStandardOutput() {
        String output = "";
        try {
            output = IOUtils.toString(process.getInputStream(), Charset.forName("UTF-8"));
        } catch (IOException | NullPointerException ignored) {
        }
        return output;
    }

    @Override
    public Map<String, String> getMapOfFilesToCopy() {
        Map<String, String> stringStringMap = new HashMap<>();
        stringStringMap.put("./testData/cards.json", "cards.json");
        stringStringMap.put("./testData/heros.json", "heros.json");
        stringStringMap.put("./testData/GameServer-1.0-SNAPSHOT-jar-with-dependencies.jar", "GameServer.jar");
        return stringStringMap;
    }

    @Override
    public boolean isStartingNewProcess() {
        return true;
    }
}
