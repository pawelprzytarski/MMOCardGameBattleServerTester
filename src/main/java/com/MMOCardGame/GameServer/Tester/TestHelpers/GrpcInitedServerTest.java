package com.MMOCardGame.GameServer.Tester.TestHelpers;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.InnerServerGrpc;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.NewSessionData;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.Reply;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.UserData;
import io.grpc.*;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class GrpcInitedServerTest extends GrpcServerTest {
    @Setter
    protected List<UserData> userData;
    @Setter
    private int sessionId = 1;
    @Setter
    private int matchType = GameSessionSettings.MatchType.Single;

    @Override
    public void initServer() throws Throwable {
        super.initServer();
        Reply reply = initGameSession();
        if (reply.getErrorCode() != 201)
            throw new AssertionError("Game session cannot be created");

    }

    protected Reply initGameSession() {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", servicePort)
                .usePlaintext(true)
                .build();

        InnerServerGrpc.InnerServerBlockingStub innerServerBlockingStub = getServerBlockingStubWithAddingHeader(channel);

        NewSessionData.Builder newSessionDataOrBuilder = NewSessionData.newBuilder();
        userData.forEach(newSessionDataOrBuilder::addUsers);
        Reply reply = innerServerBlockingStub.registerGameSession(newSessionDataOrBuilder
                .setGameType(matchType)
                .setSessionId(sessionId)
                .build());

        channel.shutdown();
        try {
            channel.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
        }
        return reply;
    }


    private InnerServerGrpc.InnerServerBlockingStub getServerBlockingStubWithAddingHeader(Channel channel) {
        return InnerServerGrpc.newBlockingStub(channel)
                .withInterceptors(new ClientInterceptor() {
                    @Override
                    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, io.grpc.Channel channel1) {
                        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel1.newCall(methodDescriptor, callOptions)) {
                            @Override
                            public void start(Listener<RespT> responseListener, Metadata headers) {
                                headers.put(Metadata.Key.of("Token", Metadata.ASCII_STRING_MARSHALLER), ProcessServerProvider.ServiceToken);
                                super.start(responseListener, headers);
                            }
                        };
                    }
                });
    }
}
