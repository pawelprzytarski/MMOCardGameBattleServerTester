package com.MMOCardGame.GameServer.Tester;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestCaseWithId {
    TestCase testCase;
    int id;
}
