package com.MMOCardGame.GameServer.Tester;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) {
        Tester tester = new Tester();
        try {
            tester.start(args);
        } catch (IOException | ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
