package com.MMOCardGame.GameServer.Tester;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class TestResult {
    private String test;
    private Result result = TestResult.Result.Passed;
    private Throwable causedBy;
    private Date started;
    private long duration;
    private List<TestCaseResult> testCaseResults = new ArrayList<>();
    private int passedCases = 0;
    private int ignoredCases = 0;
    private int errorCases = 0;
    private int failedCases = 0;


    public TestResult(String test, Date started) {
        this.test = test;
        this.started = started;
    }

    public void addTestCaseResult(TestCaseResult testCaseResult) {
        testCaseResults.add(testCaseResult);
        if (testCaseResult.getResult() != Result.Passed && testCaseResult.getResult() != Result.Ignored)
            result = Result.Failed;
        switch (testCaseResult.getResult()) {
            case Error:
                errorCases++;
                break;
            case Ignored:
                ignoredCases++;
                break;
            case Failed:
                failedCases++;
                break;
            case Passed:
                passedCases++;
                break;
        }
    }

    public Result getResult() {
        if (ignoredCases == testCaseResults.size())
            return Result.Ignored;
        else return result;
    }

    enum Result {
        Passed,
        Failed,
        Error,
        Ignored,
    }
}
