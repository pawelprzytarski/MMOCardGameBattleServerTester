package com.MMOCardGame.GameServer.Tester;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Ignored {
    String reason();
}
