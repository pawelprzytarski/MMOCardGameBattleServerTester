package com.MMOCardGame.GameServer.Tester;

import lombok.Setter;

import java.util.Map;

public abstract class ServerProvider {
    @Setter
    protected int servicePort;
    @Setter
    protected int gamePort;
    @Setter
    protected String workspace;

    public abstract void runServer();

    public abstract void stopServer();

    public abstract Throwable getError();

    public abstract boolean isError();

    public abstract int exitCode();

    public abstract String getErrorOutput();

    public abstract String getStandardOutput();

    public abstract Map<String, String> getMapOfFilesToCopy();

    public abstract boolean isStartingNewProcess();
}
