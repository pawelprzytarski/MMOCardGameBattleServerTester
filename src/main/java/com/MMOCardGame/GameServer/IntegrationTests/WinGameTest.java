package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.GameResult;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.PhaseChanged;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.PlayerGameEnd;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;

import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Test
public class WinGameTest extends AfterFirstHandTestsHelper {
    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1))
                .createCases();
    }

    @Override
    public void runTest() {
        super.runTest();

        var clientsToSurrender = new Stack<GrpcClient>();
        clients.stream().skip(1).forEach(clientsToSurrender::push);
        var wonClient = clients.get(0);

        while(!clientsToSurrender.isEmpty()){
            var currentLostClient = clientsToSurrender.pop();
            currentLostClient.surrender();

            clientsToSurrender.forEach(client->{
                assertReceivePlayerDefeatMessage(currentLostClient, client);
            });
            assertReceivePlayerDefeatMessage(currentLostClient, currentLostClient);
            assertReceivePlayerDefeatMessage(currentLostClient, wonClient);
        }

        var winMessage = wonClient.waitUntil(300, PlayerGameEnd.class);

        assertEquals(GameResult.Won, winMessage.getResult());
        assertEquals(wonClient.getUserId(), winMessage.getPlayer());
    }

    private void assertReceivePlayerDefeatMessage(GrpcClient currentClient, GrpcClient client) {
        var result = client.waitUntil(300, PlayerGameEnd.class);
        assertEquals(currentClient.getUserId() , result.getPlayer());
        assertEquals(GameResult.Defeated, result.getResult());
    }
}
