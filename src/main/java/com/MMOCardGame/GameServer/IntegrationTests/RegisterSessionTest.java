package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.GameEngine.Sessions.GameSessionSettings;
import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.InnerServerGrpc;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.NewSessionData;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.Reply;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.UserData;
import com.MMOCardGame.GameServer.Tester.ServerProvider;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCase;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcServerTest;
import com.MMOCardGame.GameServer.Tester.TestHelpers.ProcessServerProvider;
import io.grpc.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Test
public class RegisterSessionTest extends GrpcServerTest {
    private int usersCount;

    @TestCasesList
    public static List<TestCase> getCasesList() {
        return Arrays.asList(new SpecialCase(2), new SpecialCase(3), new SpecialCase(4));
    }

    @Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }

    @Override
    public void initCase(TestCase testCase) {
        usersCount = ((SpecialCase) testCase).usersCount;
    }

    @Override
    public void runTest() {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", servicePort)
                .usePlaintext(true)
                .build();

        InnerServerGrpc.InnerServerBlockingStub innerServerBlockingStub = getServerBlockingStubWithAddingHeader(channel);

        NewSessionData.Builder newSessionDataOrBuilder = NewSessionData.newBuilder();

        for (int i = 1; i <= usersCount; i++)
            newSessionDataOrBuilder.addUsers(getUserInfo(i));

        Reply reply = innerServerBlockingStub.registerGameSession(newSessionDataOrBuilder
                .setGameType(GameSessionSettings.MatchType.Single)
                .setSessionId(1)
                .build());

        channel.shutdown();
        try {
            channel.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
        }

        assertEquals(Reply.newBuilder().setErrorCode(ErrorCodes.Created).build(), reply);
    }

    private UserData getUserInfo(int i) {
        return UserData.newBuilder()
                .addAllCardIds(getCardIds())
                .setToken("TestToken" + i)
                .setUsername("User" + i)
                .setHeroId(1)
                .build();
    }

    public List<Integer> getCardIds() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 45; i++)
            list.add(i % 5 + 1);
        return list;
    }

    private InnerServerGrpc.InnerServerBlockingStub getServerBlockingStubWithAddingHeader(Channel channel) {
        return InnerServerGrpc.newBlockingStub(channel)
                .withInterceptors(new ClientInterceptor() {
                    @Override
                    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, io.grpc.Channel channel1) {
                        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel1.newCall(methodDescriptor, callOptions)) {
                            @Override
                            public void start(Listener<RespT> responseListener, Metadata headers) {
                                headers.put(Metadata.Key.of("Token", Metadata.ASCII_STRING_MARSHALLER), ProcessServerProvider.ServiceToken);
                                super.start(responseListener, headers);
                            }
                        };
                    }
                });
    }

    static class SpecialCase extends TestCase {
        int usersCount;

        public SpecialCase(int usersCount) {
            super("Users count: " + usersCount, false);
            this.usersCount = usersCount;
        }
    }
}


