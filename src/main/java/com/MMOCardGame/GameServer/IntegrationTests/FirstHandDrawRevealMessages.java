package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardPosition;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardRevealed;
import com.MMOCardGame.GameServer.Tester.AssertionException;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.BadBehaviourException;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Test
public class FirstHandDrawRevealMessages extends EndWaitingTest {
    private final int cardsInFirstHandCount = 7;

    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addFilter(caseArgs -> !(caseArgs.getUserCount() == 3 && caseArgs.getMatchType() == 2))
                .createCases();
    }

    @Override
    public void runTest() {
        super.runTest();

        clients.forEach(client -> {
            Set<Integer> cards = new HashSet<>();
            for (int i = 0; i < cardsInFirstHandCount; i++) {
                CardRevealed cardRevealed = client.waitUntil(timeoutMillis, CardRevealed.class);
                assertNotEquals(-1, cardRevealed.getCardId());
                assertNotEquals(-1, cardRevealed.getInstanceId());
                assertEquals(client.getUserId(), cardRevealed.getOwner());
                assertEquals(CardPosition.Hand, cardRevealed.getPosition());
                cards.add(cardRevealed.getInstanceId());
            }
            assertEquals(cardsInFirstHandCount, cards.size());
            try {
                client.waitUntil(100, CardRevealed.class);
                throw new BadBehaviourException("Not excepted new messages");
            } catch (AssertionException e) {
            } catch (BadBehaviourException e) {
                throw new AssertionException(e.getMessage());
            }
        });
    }
}
