package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Tester.AssertionException;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Test
public class AfterFirstHandTestsHelper extends EndWaitingTest {
    private final int cardsInFirstHandCount = 7;

    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addFilter(caseArgs -> !(caseArgs.getUserCount() == 3 && caseArgs.getMatchType() == 2))
                .createCases();
    }

    @Override
    public void runTest() {
        super.runTest();

        skipFirstHandMessages();
    }

    protected void skipFirstHandMessages() {
        consumePreviousMessages();
        endFirstHandPhase();
    }

    protected void endFirstHandPhase() {
        clients.forEach(client -> {
            SimpleResponse changeResponse = client.endPhase(PhaseChanged.newBuilder()
                    .setNewPhase(GamePhase.Untap)
                    .setNewPlayer(-1)
                    .setOldPlayer(-1)
                    .setOldPhase(GamePhase.FirstHandDraw)
                    .build());
            assertEquals(0, changeResponse.getResponseCode());
        });

        clients.forEach(client -> {
            PhaseChanged phaseChanged = client.waitUntil(timeoutMillis, PhaseChanged.class);
            assertEquals(GamePhase.Untap, phaseChanged.getNewPhase());
        });
    }

    protected void consumePreviousMessages() {
        clients.forEach(client -> {
            for(int i=0;i<clients.size();i++)
                client.waitUntil(100, HandChanged.class);
        });
    }
}
