package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Tester.AssertionException;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCase;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;
import lombok.ToString;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Test
public class FirstHandDrawMuligan extends EndWaitingTest {
    private final int cardsInFirstHandCount = 7;
    private final int muligansMaxCount = cardsInFirstHandCount + 1;
    private MuliganArgs args;

    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<MuliganArgs> getMuliganCases() {
        return new CasesCreator<>(MuliganArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addParam("testAllMuligans", Arrays.asList(true, false))
                .addFilter(caseArgs -> !(caseArgs.getUserCount() == 3 && caseArgs.getMatchType() == 2))
                .createCases();
    }

    @Override
    public void initCase(TestCase testCase) {
        super.initCase(testCase);
        args = (MuliganArgs) testCase;
    }

    @Override
    public void runTest() {
        super.runTest();
        GrpcClient replacingClient = clients.get(0);
        List<GrpcClient> otherClients = clients.stream()
                .filter(grpcClient -> grpcClient != replacingClient)
                .collect(Collectors.toList());

        clients.forEach(client->{
            for(int i=0;i<clients.size();i++){
                client.waitUntil(100, HandChanged.class);
            }
        });


        testMuligan(replacingClient, 1);
        testOtherPlayerReceiveMuliganMessages(replacingClient, otherClients, 1);

        if (args.testAllMuligans)
            testAllPossibleMuligans(replacingClient, otherClients);
        else testAcceptMuligan(replacingClient);

        testUnallowedMuligans(replacingClient);
        testEndPhaseAfterMuligan(otherClients);
    }

    private void testAcceptMuligan(GrpcClient replacingClient) {
        SimpleResponse changeResponse = replacingClient.endPhase(PhaseChanged.newBuilder()
                .setNewPhase(GamePhase.Untap)
                .setNewPlayer(-1)
                .setOldPlayer(-1)
                .setOldPhase(GamePhase.FirstHandDraw)
                .build());
        assertEquals(0, changeResponse.getResponseCode());
    }

    protected void testEndPhaseAfterMuligan(List<GrpcClient> otherClients) {
        otherClients.forEach(client -> {
            SimpleResponse changeResponse = client.endPhase(PhaseChanged.newBuilder()
                    .setNewPhase(GamePhase.Untap)
                    .setNewPlayer(-1)
                    .setOldPlayer(-1)
                    .setOldPhase(GamePhase.FirstHandDraw)
                    .build());
            assertEquals(0, changeResponse.getResponseCode());
        });

        clients.forEach(client -> {
            PhaseChanged phaseChanged = client.waitUntil(timeoutMillis, PhaseChanged.class);
            assertEquals(GamePhase.Untap, phaseChanged.getNewPhase());
        });
    }

    protected void testUnallowedMuligans(GrpcClient replacingClient) {
        SimpleResponse response1 = replacingClient.muligan();
        assertEquals(ErrorCodes.InconsistentClientState, response1.getResponseCode());
    }

    protected void testAllPossibleMuligans(GrpcClient replacingClient, List<GrpcClient> otherClients) {
        for (int i = 2; i <= muligansMaxCount; i++) {
            testMuligan(replacingClient, i);
            testOtherPlayerReceiveMuliganMessages(replacingClient, otherClients, i);
        }
    }

    protected void testOtherPlayerReceiveMuliganMessages(GrpcClient replacingClient, List<GrpcClient> otherClients, int muliganCount1) {
        otherClients.forEach(client -> {
            HandChanged handChange = client.waitUntil(timeoutMillis, HandChanged.class);
            assertEquals(CardChangeType.Remove, handChange.getType());
            assertEquals(replacingClient.getUserId(), handChange.getPlayer());
            assertEquals(getExpectedRemovedCardsForMuligan(muliganCount1), handChange.getCount());
            assertEquals(0, handChange.getCardInstancesList().size());

            HandChanged handChange2 = client.waitUntil(timeoutMillis, HandChanged.class);
            assertEquals(CardChangeType.Draw, handChange2.getType());
            assertEquals(replacingClient.getUserId(), handChange.getPlayer());
            assertEquals(muligansMaxCount - muliganCount1, handChange2.getCount());
            assertEquals(0, handChange2.getCardInstancesList().size());
        });
    }

    private int getExpectedRemovedCardsForMuligan(int muliganCount1) {
        return muliganCount1 == 1 ? cardsInFirstHandCount : muligansMaxCount - muliganCount1 + 1;
    }

    protected void testMuligan(GrpcClient replacingClient, int muliganCount) {
        var simpleReponse = replacingClient.muligan();
/*
        try {
            replacingClient.consumeMessages(100, 1000);

        }catch (Exception e){
            throw e;
        }*/

        var tt=replacingClient.waitUntil(100, HandChanged.class);
        var handChanged=replacingClient.waitUntil(100, HandChanged.class);

        assertEquals(0, simpleReponse.getResponseCode());
        assertEquals(CardChangeType.Draw, handChanged.getType());
        assertEquals(replacingClient.getUserId(), handChanged.getPlayer());
        assertEquals(muligansMaxCount - muliganCount, handChanged.getCount());
        assertEquals(muligansMaxCount - muliganCount, handChanged.getCardInstancesList().size());
    }

    @ToString(callSuper = true)
    public static class MuliganArgs extends CaseArgs {
        public boolean testAllMuligans;
    }
}
