package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.CardChangeType;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.HandChanged;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Test
public class FirstHandDrawMessages extends EndWaitingTest {
    private final int cardsInFirstHandCount = 7;

    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addFilter(caseArgs -> !(caseArgs.getUserCount() == 3 && caseArgs.getMatchType() == 2))
                .createCases();
    }

    @Override
    public void runTest() {
        super.runTest();

        clients.forEach(client -> {
            Set<Integer> listOfPlayers = new HashSet<>();
            for (int i = 0; i < clients.size(); i++) {
                HandChanged handChange = client.waitUntil(timeoutMillis, HandChanged.class);
                listOfPlayers.add(handChange.getPlayer());
                assertHandChangedMessage(client, handChange);
            }
            assertEquals(clients.stream().map(GrpcClient::getUserId).collect(Collectors.toSet()), listOfPlayers);
        });
    }

    protected void assertHandChangedMessage(GrpcClient client, HandChanged handChange) {
        if (handChange.getPlayer() == client.getUserId())
            assertEquals(cardsInFirstHandCount, handChange.getCardInstancesList().size());
        else {
            assertEquals(cardsInFirstHandCount, handChange.getCount());
            assertEquals(0, handChange.getCardInstancesList().size());
        }
        assertEquals(CardChangeType.Draw, handChange.getType());
    }
}
