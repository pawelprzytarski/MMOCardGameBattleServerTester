package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.GamePhase;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.PhaseChanged;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.SimpleResponse;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;
import com.MMOCardGame.GameServer.Tester.TestHelpers.InitedClientsTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Test
public class EndWaitingIncorrectOldPhaseTest extends InitedClientsTest {

    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addFilter(caseArgs -> !(caseArgs.getUserCount() == 3 && caseArgs.getMatchType() == 2))
                .createCases();
    }

    @Override
    public void runTest() {
        clients.forEach(client -> {
            SimpleResponse response = client.endPhase(PhaseChanged.newBuilder()
                    .setOldPhase(GamePhase.Main)
                    .setNewPhaseValue(-1)
                    .setOldPlayer(-1)
                    .setNewPlayer(-1)
                    .build());
            assertEquals(ErrorCodes.InconsistentClientState, response.getResponseCode());
        });

        GamePhase currentPhase = clients.get(0).getAllGameState().getCurrentPhase();
        assertEquals(GamePhase.Waiting, currentPhase);

        clients.forEach(GrpcClient::verifyNoMessages);
    }
}
