package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Tester.Main;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Test
public class PhasesChangesTest extends AfterFirstHandTestsHelper {
    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1))
                .createCases();
    }

    @Override
    public void runTest() {
        super.runTest();

        int startingPlayer = clients.get(0).getCurrectPlayer();

        var playersWithRounds = new HashSet<Integer>();

        for(int i=0;i<clients.size();i++){
            int currentStartingPlayer=clients.get(0).getCurrectPlayer();
            playersWithRounds.add(currentStartingPlayer);
            testOnePlayerRound();
        }

        assertEquals(clients.size(), playersWithRounds.size());

        for (var client: clients) {
            assertEquals(startingPlayer, client.getCurrectPlayer());
            assertEquals(GamePhase.Untap, client.getCurrentPhase());
        }
    }

    private void testOnePlayerRound() {
        var roundClient = getPhaseClient();
        var otherClients = getOtherClients(roundClient);

        endPhaseAndAssertPhaseChanged(roundClient, otherClients, GamePhase.Untap, GamePhase.Main);
        endPhaseAndAssertPhaseChanged(roundClient, otherClients, GamePhase.Main, GamePhase.Attack);
        endPhaseAndAssertPhaseChanged(roundClient, otherClients, GamePhase.Attack, GamePhase.Defence);
        for(int i=0;i<otherClients.size()-1;i++)
            endPhaseAndAssertPhaseChanged(getPhaseClient(), getOtherClients(getPhaseClient()), GamePhase.Defence, GamePhase.Defence);
        endPhaseAndAssertPhaseChanged(getPhaseClient(), getOtherClients(getPhaseClient()), GamePhase.Defence, GamePhase.ReactionPhase);
        for(int i=0;i<otherClients.size();i++)
            endPhaseAndAssertPhaseChanged(getPhaseClient(), getOtherClients(getPhaseClient()), GamePhase.ReactionPhase, GamePhase.ReactionPhase);
        endPhaseAndAssertPhaseChanged(getPhaseClient(), getOtherClients(getPhaseClient()), GamePhase.ReactionPhase, GamePhase.AttackExecutionPhase);
        for(var client: clients) {
            var resultPhase = client.waitUntil(100, PhaseChanged.class).getNewPhase();
            assertEquals(GamePhase.EndRound, resultPhase);
        }
        for(int i=0;i<otherClients.size();i++)
            endPhaseAndAssertPhaseChanged(getPhaseClient(), getOtherClients(getPhaseClient()), GamePhase.EndRound, GamePhase.EndRound);
        endPhaseAndAssertPhaseChanged(getPhaseClient(), getOtherClients(getPhaseClient()), GamePhase.EndRound, GamePhase.Untap);

        assertNotEquals(roundClient.getUserId(), roundClient.getCurrectPlayer());
    }

    private List<GrpcClient> getOtherClients(GrpcClient roundClient) {
        return clients.stream().filter(client -> client.getUserId()!=roundClient.getUserId()).collect(Collectors.toList());
    }

    private void endPhaseAndAssertPhaseChanged(GrpcClient roundClient, List<GrpcClient> otherClients, GamePhase untap, GamePhase main) {
        endPhaseAndAssert(roundClient, untap, main);
        for (var client : otherClients) {
            var result = client.waitUntil(100, PhaseChanged.class);
            assertEquals(main, result.getNewPhase());
        }
    }

    private GrpcClient getPhaseClient() {
        int currentPlayer=clients.get(0).getCurrectPlayer();
        return clients.stream().filter(client -> client.getUserId()==currentPlayer).findAny().orElseThrow();
    }

    private void endPhaseAndAssert(GrpcClient roundClient, GamePhase currentPhase, GamePhase expectedPhaseAfter) {
        endPlayerPhase(roundClient, currentPhase);
        var result = roundClient.waitUntil(500,PhaseChanged.class);
        assertEquals(expectedPhaseAfter, result.getNewPhase());
    }

    private void endPlayerPhase(GrpcClient roundClient, GamePhase gamePhase) {
        var response = roundClient.endPhase(PhaseChanged.newBuilder()
                .setOldPhase(gamePhase)
                .setOldPlayer(roundClient.getUserId())
                .build());

        assertEquals(0, response.getResponseCode());
    }
}
