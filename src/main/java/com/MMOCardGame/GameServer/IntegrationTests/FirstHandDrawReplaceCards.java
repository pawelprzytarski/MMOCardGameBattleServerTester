package com.MMOCardGame.GameServer.IntegrationTests;

import com.MMOCardGame.GameServer.Servers.ErrorCodes;
import com.MMOCardGame.GameServer.Servers.ProtoBuffers.*;
import com.MMOCardGame.GameServer.Tester.AssertionException;
import com.MMOCardGame.GameServer.Tester.Test;
import com.MMOCardGame.GameServer.Tester.TestCasesList;
import com.MMOCardGame.GameServer.Tester.TestHelpers.CasesCreator;
import com.MMOCardGame.GameServer.Tester.TestHelpers.GrpcClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Test
public class FirstHandDrawReplaceCards extends EndWaitingTest {
    private final int cardsInFirstHandCount = 7;

    /*@Override
    protected ServerProvider initServerProvider() {
        return getProcessServerProvider();
    }*/

    @TestCasesList
    public static List<CaseArgs> getCases() {
        return new CasesCreator<>(CaseArgs.class)
                .addParam("userCount", Arrays.asList(2, 3, 4))
                .addParam("matchType", Arrays.asList(1, 2))
                .addFilter(caseArgs -> !(caseArgs.getUserCount() == 3 && caseArgs.getMatchType() == 2))
                .createCases();
    }

    @Override
    public void runTest() {
        super.runTest();
        GrpcClient replacingClient = clients.get(0);
        List<GrpcClient> otherClients = clients.stream()
                .filter(grpcClient -> grpcClient != replacingClient)
                .collect(Collectors.toList());

        List<Integer> cards = consumePreviousMessagesAndGetCardsForReplaceClient(replacingClient, otherClients);

        testReplaceCardsRequest(replacingClient, cards);
        testMessagesReceivedByOtherClients(replacingClient, otherClients);
        testSecondReplaceRequest(replacingClient, cards);
        testEndPhaseAfterReplaceCardRequest(otherClients);
    }

    protected void testEndPhaseAfterReplaceCardRequest(List<GrpcClient> otherClients) {
        otherClients.forEach(client -> {
            SimpleResponse changeResponse = client.endPhase(PhaseChanged.newBuilder()
                    .setNewPhase(GamePhase.Untap)
                    .setNewPlayer(-1)
                    .setOldPlayer(-1)
                    .setOldPhase(GamePhase.FirstHandDraw)
                    .build());
            assertEquals(0, changeResponse.getResponseCode());
        });

        clients.forEach(client -> {
            PhaseChanged phaseChanged = client.waitUntil(timeoutMillis, PhaseChanged.class);
            assertEquals(GamePhase.Untap, phaseChanged.getNewPhase());
        });
    }

    protected void testSecondReplaceRequest(GrpcClient replacingClient, List<Integer> cards) {
        SimpleResponse response = replacingClient.replaceCards(HandChanged.newBuilder()
                .setPlayer(replacingClient.getUserId())
                .setType(CardChangeType.Remove)
                .addCardInstances(cards.get(3))
                .addCardInstances(cards.get(4))
                .addCardInstances(cards.get(5))
                .build());
        assertEquals(ErrorCodes.InconsistentClientState, response.getResponseCode());
    }

    protected void testMessagesReceivedByOtherClients(GrpcClient replacingClient, List<GrpcClient> otherClients) {
        otherClients.forEach(client -> {
            HandChanged handChange = client.waitUntil(timeoutMillis, HandChanged.class);
            assertEquals(CardChangeType.Remove, handChange.getType());
            assertEquals(replacingClient.getUserId(), handChange.getPlayer());
            assertEquals(3, handChange.getCount());
            assertEquals(0, handChange.getCardInstancesList().size());

            HandChanged handChange2 = client.waitUntil(timeoutMillis, HandChanged.class);
            assertEquals(CardChangeType.Draw, handChange2.getType());
            assertEquals(replacingClient.getUserId(), handChange.getPlayer());
            assertEquals(3, handChange2.getCount());
            assertEquals(0, handChange2.getCardInstancesList().size());
        });
    }

    protected void testReplaceCardsRequest(GrpcClient replacingClient, List<Integer> cards) {
        SimpleResponse response = replacingClient.replaceCards(HandChanged.newBuilder()
                .setPlayer(replacingClient.getUserId())
                .setType(CardChangeType.Remove)
                .addCardInstances(cards.get(0))
                .addCardInstances(cards.get(1))
                .addCardInstances(cards.get(2))
                .build());

        replacingClient.waitUntil(100, HandChanged.class);
        HandChanged handChanged=replacingClient.waitUntil(100, HandChanged.class);

        assertEquals(0, response.getResponseCode());
        assertEquals(CardChangeType.Draw, handChanged.getType());
        assertEquals(replacingClient.getUserId(), handChanged.getPlayer());
        assertEquals(3, handChanged.getCount());
        assertEquals(3, handChanged.getCardInstancesList().size());
    }

    protected List<Integer> consumePreviousMessagesAndGetCardsForReplaceClient(GrpcClient replacingClient, List<GrpcClient> otherClients) {
        otherClients.forEach(client -> {
            for(int i=0;i<clients.size();i++)
                client.waitUntil(100, HandChanged.class);
        });

        List<Integer> cards = new ArrayList<>();
        for (int i = 0; i < cardsInFirstHandCount; i++) {
            CardRevealed cardRevealed = replacingClient.waitUntil(timeoutMillis, CardRevealed.class);
            cards.add(cardRevealed.getInstanceId());
        }
        try {
            replacingClient.consumeMessages(100, 1000);
        } catch (AssertionException e) {
        }
        return cards;
    }
}
